from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


class Contact(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    subject = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    message = models.TextField()
    when = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.name


class RequestQuote(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    category = models.CharField(max_length=50)
    budget = models.CharField(max_length=50)
    project = models.TextField()
    when = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.name