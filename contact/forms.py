from django import forms
from contact.models import Contact, RequestQuote


class ContactForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Full Name'}))
    email = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Email'}))
    subject = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Subject'}))
    phone = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Phone'}))
    message = forms.CharField(widget=forms.Textarea(
        attrs={'class': 'form-control', 'placeholder': 'Message'}))

    class Meta:
        model = Contact
        exclude = ['when']


CATEGORY_CHOICES = (
    ('', 'Select your Project Category'),
    ('web design', 'Website Design'),
    ('web dev', 'Web Development'),
    ('mobile app', 'Mobile Application'),
    ('software dev', 'Desktop Application'),
    ('E-commerce', 'E-commerce'),
    ('SEO-marketting', 'SEO/Online Marketting'),
    ('ICT', 'ICT Consulting'),
    ('domain hosting', 'Domain Name and Hosting'),
    ('others', 'Others'),
)


BUDGET_CHOICES = (
    ('', 'Select Your Budget'),
    ('120,000-200,000', '120,000 - 200,000'),
    ('250,000-450,000', '250,000 - 450,000'),
    ('500,000-1,000,000', '500,000 - 1,000,000'),
    ('2,000,000-5,000,000', '2,000,000 - 5,000,000'),
    ('above 5,000,000', 'Above 5,000,000')
)


class RequestQuoteForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Full Name'}))
    email = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Email'}))
    phone = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Phone'}))
    category = forms.CharField(label='Select Category',
        widget=forms.Select(choices=CATEGORY_CHOICES, attrs={'class': 'form-control'}))
    budget = forms.CharField(widget=forms.Select(choices=BUDGET_CHOICES,
        attrs={'class': 'form-control', 'placeholder': 'Subject'}))
    project = forms.CharField(widget=forms.Textarea(
        attrs={'class': 'form-control', 'placeholder': 'Project Details'}))

    class Meta:
        model = RequestQuote
        exclude = ['when']
