from django.contrib import admin
from contact.models import Contact, RequestQuote


class ContactAdmin(admin.ModelAdmin):
    list_display = ["name", "email", "phone", "subject"]

    class Meta:
        models = Contact


admin.site.register(Contact, ContactAdmin)


@admin.register(RequestQuote)
class RequestQuoteAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'category', 'budget']
