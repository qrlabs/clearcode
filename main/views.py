from django.shortcuts import render
from django.core.mail import send_mail
from contact.forms import ContactForm, RequestQuoteForm


def home(request):
    if "contact_form" in request.POST:
        form = ContactForm(request.POST)
        if form.is_valid():
            contact = form.save(commit=False)
            contact.save()
            recieve_email = contact.email
            send_email = 'info@clearcode.com.ng'
            subject = "Clear Code Labs: We would get back to you shortly"
            message = "We have recieved your message. Our support team is working on it and would send you reply as soon as we conclude on the issue raised"
            send_mail(
                subject,
                message,
                send_email,
                [recieve_email],
                fail_silently=False,
                # html_message=html_message
                )
    else:
        form = ContactForm()

    if "request_quote_form" in request.POST:
        form2 = RequestQuoteForm(request.POST)
        if form2.is_valid():
            req = form2.save(commit=False)
            req.save()
            recieve_email = req.email
            send_email = 'info@clearcode.com.ng'
            subject = 'Clear Code Labs: Your project quote is been worked on'
            message = 'We have received your quote and would give you feed back on the details for the implementation of the project'
            send_mail(
                subject,
                message,
                send_email,
                [recieve_email],
                fail_silently=False,
                # html_message=html_message
                )
    else:
        form2 = RequestQuoteForm()
    context = {
        "form": form,
        "form2": form2,
    }
    return render(request, 'home.html', context)
