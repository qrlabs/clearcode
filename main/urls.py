from django.conf.urls import include, url

from .views import home


urlpatterns = [
    url(r'^$', home, name="home"),
    # url(r'^service/$', service, name="service"),
    # url(r'^about/$', about, name="about"),
]
